import aseprite.Aseprite;
import game.Game;
import lime.ui.GamepadButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import openfl.Assets;

class Instructions extends State {
	var anim:Aseprite;

	public function new() {
		super();

		var sufix:String = switch (Main.instance.lastInputMethod) {
			case KEYBOARD: 'kb';
			case GAMEPAD: 'gp';
		};

		anim = Aseprite.fromBytes(Assets.getBytes('assets/instructions_$sufix.aseprite'));

		anim.play(1, () -> {
			proceed();
		});

		addChild(anim);
	}

	function proceed() {
		anim.pause();
		Main.instance.setState(new Game());
	}

	override function gamepadButtonDown(button:GamepadButton) {
		proceed();
	}

	override function keyDown(keyCode:KeyCode, keyModifier:KeyModifier) {
		proceed();
	}
}
