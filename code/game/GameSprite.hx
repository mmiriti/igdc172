package game;

import openfl.display.BitmapData;
import raycaster.Sprite;

class GameSprite extends Sprite {
	public var remove:Bool = false;

	public function new(from:BitmapData, ?x:Float, ?y:Float) {
		super(from, x, y);
	}

	public function update(delta:Float) {
	}
}
