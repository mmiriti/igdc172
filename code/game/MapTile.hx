package game;

import openfl.display.BitmapData;

class MapTile {
	public var wallTexture:BitmapData;
	public var ceilTexture:BitmapData;
	public var floorTexture:BitmapData;
	public var collision:Bool;

	public function new() {}
}
