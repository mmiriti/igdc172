package game;

import aseprite.Aseprite;
import openfl.display.BitmapData;
import openfl.utils.Assets;

class AnimSprite extends GameSprite {
	var aseprite:Aseprite;

	static var aseCache:Map<String, Aseprite> = [];

	override function get_bitmapData():BitmapData {
		return aseprite.bitmap.bitmapData;
	}

	public function new(asset:String, ?x:Float, ?y:Float) {
		super(null, x, y);

		if (aseCache.exists(asset)) {
			aseprite = aseCache[asset].spawn();
		} else {
			aseprite = Aseprite.fromBytes(Assets.getBytes(asset), false);
			aseCache[asset] = aseprite;
		}
	}

	override public function update(delta:Float) {
		aseprite.advance(Std.int(delta * 1000));
	}
}
