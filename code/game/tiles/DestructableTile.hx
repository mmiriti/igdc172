package game.tiles;

import openfl.Assets;

class DestructableTile extends MapTile {
	var hitPoints:Int = 1;

	public function hit() {
		switch (hitPoints) {
			case 1:
				wallTexture = Assets.getBitmapData('assets/textures/bricks-cracked.png');
				hitPoints--;
			case 0:
				wallTexture = null;
				collision = false;
		}
	}
}
