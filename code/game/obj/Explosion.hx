package game.obj;

class Explosion extends AnimSprite {
	public function new(?x:Float, ?y:Float) {
		super('assets/explosion.ase', x, y);

		aseprite.play(1, () -> {
			raycaster.removeSprite(this);
		});
	}
}
