package game.obj;

import openfl.display.BitmapData;
import openfl.utils.Assets;

class BombPreview extends GameSprite {
	public function new(?x:Float, ?y:Float) {
		super(Assets.getBitmapData('assets/bomb.png'), x, y);
	}
}
