package game;

import game.obj.Bomb;
import game.obj.BombPreview;
import lime.ui.GamepadButton;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import menu.Pause;
import openfl.Assets;
import openfl.display.Sprite;
import openfl.geom.Point;
import raycaster.Raycaster;

using Math;
using Std;

class Game extends State {
	static final ROTATION_SPEED:Float = Math.PI;

	public static var current:Game;

	public var map:GameMap;

	var rc:Raycaster;

	var layBomb(get, never):Bool;

	var _shakeAmp:Float = 0;
	var _shakePhase:Float = 0;
	var _shakeTime:Float = 0;
	var _shakeTimeTotal:Float = 0;

	var bombPreviewSprite:BombPreview = new BombPreview();
	var cameraTraveledDistance:Float;

	var stepAlt:Int = 1;

	var putCellX:Int;
	var putCellY:Int;

	function get_layBomb():Bool
		return switch (inputMode) {
			case KEYBOARD: inputKeys[KeyCode.SPACE];
			case GAMEPAD: gamepadButtons[GamepadButton.A];
		};

	public function new() {
		super();

		current = this;

		map = GameMap.fromJson(Assets.getText('assets/levels/test.json'));

		rc = new Raycaster(40, 40, 100, map);
		rc.camera.position.setTo(2.5, 2.5);
		rc.x = rc.y = -4;
		addChild(rc);

		rc.addSprite(bombPreviewSprite);
		bombPreviewSprite.visible = false;

		var _mask = new Sprite();
		_mask.graphics.beginFill(0x0);
		_mask.graphics.drawRect(0, 0, 32, 32);
		_mask.graphics.endFill();

		mask = _mask;
		addChild(_mask);

		playSound('assets/sound/ambience.ogg', 0, 9999);
	}

	function pause() {
		pauseSounds();
		Main.instance.setState(new Pause(this));
	}

	public function shake(amp:Float, time:Float = 1) {
		_shakeAmp = amp;
		_shakeTime = _shakeTimeTotal = time;
	}

	override function keyDown(keyCode:KeyCode, keyModifier:KeyModifier) {
		if (keyCode == ESCAPE)
			pause();
		else
			super.keyDown(keyCode, keyModifier);
	}

	override function keyUp(keyCode:KeyCode, keyModifier:KeyModifier) {
		if (keyCode == SPACE && layBomb) {
			putBomb();
		}

		super.keyUp(keyCode, keyModifier);
	}

	override function gamepadButtonDown(button:GamepadButton) {
		if (button == START)
			pause();
		else
			super.gamepadButtonDown(button);
	}

	override function gamepadButtonUp(button:GamepadButton) {
		if (button == A && layBomb) {
			putBomb();
		}

		super.gamepadButtonUp(button);
	}

	function putBomb() {
		var bomb = new Bomb(putCellX + 0.5, putCellY + 0.5);
		rc.addSprite(bomb);
	}

	override function update(delta:Float) {
		if (_shakeTime > 0) {
			var _shakeT:Float = _shakeTime / _shakeTimeTotal;
			_shakeTime = Math.max(0, _shakeTime - delta);
			_shakePhase += Math.PI * 100 * Math.random() * delta;

			var tamp:Float = 8 * _shakeAmp;

			rc.x = Math.floor(-(tamp / 2) + tamp * _shakeT * Math.sin(_shakePhase));
			rc.y = Math.floor(-(tamp / 2) + tamp * _shakeT * Math.cos(_shakePhase));
		}

		for (sprite in rc.sprites) {
			if (sprite.is(GameSprite)) {
				cast(sprite, GameSprite).update(delta);
				if (cast(sprite, GameSprite).remove) {
					rc.sprites.remove(sprite);
				}
			}
		}

		var p = rc.camera.position.clone();

		rc.camera.angle += ROTATION_SPEED * inputDirectionX() * delta;
		rc.camera.position.x += rc.camera.direction.x * 2 * inputMovementY() * delta;
		rc.camera.position.y += rc.camera.direction.y * 2 * inputMovementY() * delta;

		rc.camera.position.x -= Math.cos(rc.camera.angle - Math.PI / 2) * 2 * inputMovementX() * delta;
		rc.camera.position.y -= Math.sin(rc.camera.angle - Math.PI / 2) * 2 * inputMovementX() * delta;

		rc.cameraCollision();

		var dp = new Point(rc.camera.position.x - p.x, rc.camera.position.y - p.y);

		cameraTraveledDistance += dp.length.abs();

		if (cameraTraveledDistance >= 0.8) {
			playSound('assets/sound/step_${stepAlt}.ogg');
			stepAlt = stepAlt == 1 ? 2 : 1;
			cameraTraveledDistance = 0;
		}

		if (layBomb) {
			putCellX = Std.int(rc.camera.position.x + rc.camera.direction.x);
			putCellY = Std.int(rc.camera.position.y + rc.camera.direction.y);

			var tile = rc.map.getTile(putCellX, putCellY);

			if (tile == null || tile.collision) {
				putCellX = Std.int(rc.camera.position.x);
				putCellY = Std.int(rc.camera.position.y);
			}

			bombPreviewSprite.x = putCellX + 0.5;
			bombPreviewSprite.y = putCellY + 0.5;
			bombPreviewSprite.visible = true;
		} else {
			bombPreviewSprite.visible = false;
		}
	}

	override function render() {
		rc.render();
	}
}
