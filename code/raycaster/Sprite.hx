package raycaster;

import openfl.display.BitmapData;

class Sprite {
	public var x:Float;
	public var y:Float;

	public var visible:Bool = true;

	public var distance:Float;
	public var screenX:Int;
	public var screenY:Int;

	@:allow(raycaster.Raycaster)
	private var _raycaster:Raycaster;

	public var raycaster(get, never):Raycaster;

	function get_raycaster():Raycaster
		return _raycaster;

	private var _bitmapData:BitmapData;

	public var bitmapData(get, set):BitmapData;

	function get_bitmapData():BitmapData
		return _bitmapData;

	function set_bitmapData(value:BitmapData):BitmapData
		return _bitmapData = value;

	public function new(?from:BitmapData, ?x:Float, y:Float) {
		this._bitmapData = from;
		this.x = x;
		this.y = y;
	}
}
