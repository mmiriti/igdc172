package raycaster;

import openfl.display.BitmapData;

typedef MapSize = {
	mapWidth:Int,
	mapHeight:Int
};

typedef MapTileDesc = {
	wallTexture:BitmapData,
	floorTexture:BitmapData,
	ceilTexture:BitmapData,
	collision:Bool
};

interface MapInterface {
	public var defaultWallTexture:BitmapData;
	public var defaultFloorTexture:BitmapData;
	public var defaultCeilTexture:BitmapData;

	function getSize():MapSize;

	function getTile(cellX:Int, cellY:Int):MapTileDesc;
}
