package menu;

import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.GamepadButton;
import openfl.system.System;

class Pause extends Menu {
	var backTo:State;

	public function new(backTo:State) {
		super();

		this.backTo = backTo;

		items = [new MenuItem('resume', resume)];

		#if sys
		items.push(new MenuItem('quit', () -> {
			System.exit(0);
		}));
		#end

		selectedItem = 0;
	}

	function resume() {
		Main.instance.setState(backTo);
	}

	override function gamepadButtonDown(button:GamepadButton) {
		if (button == START)
			resume();

		super.gamepadButtonDown(button);
	}

	override function keyDown(keyCode:KeyCode, keyModifier:KeyModifier) {
		if (keyCode == ESCAPE)
			resume();

		super.keyDown(keyCode, keyModifier);
	}
}
