package menu;

import game.Game;
import openfl.system.System;

class MainMenu extends Menu {
	public function new() {
		super();

		items = [
			new MenuItem('start', () -> {
				Main.instance.setState(new Instructions());
			})
		];

		#if sys
		items.push(new MenuItem('quit', () -> {
			System.exit(0);
		}));
		#end

		selectedItem = 0;
	}
}
