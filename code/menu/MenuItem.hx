package menu;

import aseprite.Aseprite;
import openfl.Assets;

class MenuItem {
	static var lib:Aseprite;

	public var sprite:Aseprite;

	public var selected:Bool;

	public var action:Void->Void;

	public function new(label:String, action:Void->Void) {
		if (lib == null)
			lib = Aseprite.fromBytes(Assets.getBytes('assets/ui/labels.aseprite'), false);

		sprite = lib.spawn(label, false);
		this.action = action;
	}
}
