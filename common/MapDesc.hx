typedef TileDesc = {
	wallTextureID:Null<Int>,
	floorTextureID:Null<Int>,
	ceilTextureID:Null<Int>,
	collision:Bool
};

typedef MapDesc = {
	mapWidth:Int,
	mapHeight:Int,
	player:{
		pos:{x:Float, y:Float}
	},
	textures:Array<String>,
	defaultFloorTextureID:Null<Int>,
	defaultCeilTextureID:Null<Int>,
	tiles:Array<Array<TileDesc>>,
	mobs:Array<{type:String, x:Float, y:Float}>
};
