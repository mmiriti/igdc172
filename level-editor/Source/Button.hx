import openfl.events.MouseEvent;
import openfl.text.TextField;
import openfl.display.Sprite;

class Button extends Sprite {
	public function new(title:String, action:Void->Void) {
		super();

		var caption = new TextField();
		caption.autoSize = LEFT;
		caption.selectable = false;
		caption.text = title;
		caption.textColor = 0xffffff;

		graphics.beginFill(0x0000ff);
		graphics.drawRoundRect(0, 0, caption.width + 20, caption.height + 20, 10);
		graphics.endFill();

		caption.x = (width - caption.width) / 2;
		caption.y = (height - caption.height) / 2;
		addChild(caption);

		buttonMode = true;

		addEventListener(MouseEvent.CLICK, (event:MouseEvent) -> {
			action();
		});
	}
}
