package;

import haxe.Json;
import lime.ui.FileDialog;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.text.TextField;
import sys.io.File;
import MapDesc;
import MapDesc.TileDesc;

using haxe.io.Path;
using sys.FileSystem;

class TileEdit extends Sprite {
	public static final WIDTH:Int = 50;
	public static final HEIGHT:Int = 50;

	public var tileDesc:TileDesc = {
		wallTextureID: null,
		ceilTextureID: null,
		floorTextureID: null,
		collision: false
	};

	var editor:Main;

	var floorBitmap:Bitmap = null;
	var wallBitmap:Bitmap = null;
	var ceilBitmap:Bitmap = null;

	var floorBitmapLayer:Sprite = new Sprite();
	var wallBitmapLayer:Sprite = new Sprite();
	var ceilBitmapLayer:Sprite = new Sprite();

	var cursorLayer:Sprite = new Sprite();

	public function new(editor:Main) {
		super();

		this.editor = editor;

		graphics.beginFill(0xcccccc);
		graphics.drawRect(0, 0, WIDTH, HEIGHT);
		graphics.endFill();

		buttonMode = true;

		addChild(floorBitmapLayer);
		addChild(wallBitmapLayer);
		addChild(ceilBitmapLayer);

		ceilBitmapLayer.alpha = 0.5;

		addChild(cursorLayer);

		cursorLayer.graphics.beginFill(0xff0000, 0.2);
		cursorLayer.graphics.drawRect(0, 0, WIDTH, HEIGHT);
		cursorLayer.graphics.endFill();

		cursorLayer.visible = false;

		addEventListener(MouseEvent.MOUSE_OVER, (event:MouseEvent) -> {
			cursorLayer.visible = true;
		});

		addEventListener(MouseEvent.MOUSE_OUT, (event:MouseEvent) -> {
			cursorLayer.visible = false;
		});

		addEventListener(MouseEvent.CLICK, (event:MouseEvent) -> {
			if (editor.mode == Tiles) {
				if (editor.wallTexture.selected != null) {
					tileDesc.wallTextureID = editor.textures.indexOf(editor.wallTexture.selected);
				} else {
					tileDesc.wallTextureID = null;
				}

				if (editor.floorTexture.selected != null) {
					tileDesc.floorTextureID = editor.textures.indexOf(editor.floorTexture.selected);
				} else {
					tileDesc.floorTextureID = null;
				}

				if (editor.ceilTexture.selected != null) {
					tileDesc.ceilTextureID = editor.textures.indexOf(editor.ceilTexture.selected);
				} else {
					tileDesc.ceilTextureID = null;
				}

				setTileData(tileDesc);
			}
		});
	}

	public function setTileData(tileData:TileDesc) {
		tileDesc = tileData;

		if (floorBitmap != null) {
			floorBitmapLayer.removeChild(floorBitmap);
		}

		if (tileDesc.floorTextureID != null) {
			floorBitmap = new Bitmap(editor.bitmaps[editor.textures[tileData.floorTextureID]]);
			floorBitmap.width = WIDTH;
			floorBitmap.height = HEIGHT;
			floorBitmapLayer.addChild(floorBitmap);
		}

		if (wallBitmap != null) {
			wallBitmapLayer.removeChild(wallBitmap);
		}

		if (tileDesc.wallTextureID != null) {
			wallBitmap = new Bitmap(editor.bitmaps[editor.textures[tileData.wallTextureID]]);
			wallBitmap.width = WIDTH;
			wallBitmap.height = HEIGHT;
			wallBitmapLayer.addChild(wallBitmap);

			tileData.collision = true;
		} else {
			tileData.collision = false;
		}

		if (ceilBitmap != null) {
			ceilBitmapLayer.removeChild(ceilBitmap);
		}

		if (tileDesc.ceilTextureID != null) {
			ceilBitmap = new Bitmap(editor.bitmaps[editor.textures[tileData.ceilTextureID]]);
			ceilBitmap.width = WIDTH;
			ceilBitmap.height = HEIGHT;
			ceilBitmapLayer.addChild(ceilBitmap);
		}
	}
}

class Field extends Sprite {
	public var mapWidth(default, set):Int;

	function set_mapWidth(value:Int):Int {
		if (tiles.length != value)
			tiles.resize(value);

		for (n in 0...value) {
			if (tiles[n] == null) {
				tiles[n] = [
					for (x in 0...mapHeight)
						(new TileEdit(editor))
				];

				for (ty in 0...tiles[n].length) {
					tiles[n][ty].x = n * TileEdit.WIDTH;
					tiles[n][ty].y = ty * TileEdit.HEIGHT;
					addChild(tiles[n][ty]);
				}
			}
		}

		return mapWidth = value;
	}

	public var mapHeight(default, set):Int;

	function set_mapHeight(value:Int):Int {
		for (n in 0...tiles.length) {
			if (tiles[n].length != value) {
				tiles[n].resize(value);

				for (ty in 0...tiles[n].length) {
					if (tiles[n][ty] == null) {
						tiles[n][ty] = new TileEdit(editor);
						tiles[n][ty].x = n * TileEdit.WIDTH;
						tiles[n][ty].y = ty * TileEdit.HEIGHT;
						addChild(tiles[n][ty]);
					}
				}
			}
		}

		return mapHeight = value;
	}

	var tiles:Array<Array<TileEdit>> = [];

	public var tileDescs(get, never):Array<Array<TileDesc>>;

	function get_tileDescs():Array<Array<TileDesc>> {
		return [
			for (tx in 0...tiles.length) [for (ty in 0...tiles[tx].length) tiles[tx][ty].tileDesc]
		];
	}

	var editor:Main;

	public function setTiles(tilesData:Array<Array<TileDesc>>) {
		for (tx in 0...mapWidth) {
			for (ty in 0...mapHeight) {
				tiles[tx][ty].setTileData(tilesData[tx][ty]);
			}
		}
	}

	public function new(editor:Main, initWidth:Int, initHeight:Int) {
		super();

		this.editor = editor;

		mapWidth = initWidth;
		mapHeight = initHeight;
	}
}

class TextureItem extends Sprite {
	public var texture:String;

	public var selection:Sprite = new Sprite();

	public function new(editor:Main, texture:String) {
		super();

		this.texture = texture;
		var bmp = new Bitmap(editor.bitmaps[texture]);
		bmp.width = TileEdit.WIDTH;
		bmp.height = TileEdit.HEIGHT;
		addChild(bmp);

		selection.graphics.lineStyle(2, 0xff0000);
		selection.graphics.drawRect(0, 0, TileEdit.WIDTH, TileEdit.HEIGHT);
		selection.visible = false;
		selection.mouseEnabled = false;
		addChild(selection);

		buttonMode = true;
	}
}

class Textures extends Sprite {
	var items:Array<TextureItem> = [];

	public var selected:String;

	var editor:Main;

	public function new(editor:Main, title:String, textures:Array<String>) {
		super();

		this.editor = editor;

		var label = new TextField();
		label.text = title;
		label.selectable = false;
		label.autoSize = LEFT;
		addChild(label);

		for (texture in textures) {
			var item = new TextureItem(editor, texture);
			item.y = height;
			item.addEventListener(MouseEvent.CLICK, select);
			addChild(item);
			items.push(item);
		}

		var notexture = new Sprite();
		notexture.graphics.beginFill(0xffffff);
		notexture.graphics.lineStyle(1, 0xff0000);
		notexture.graphics.drawRect(0, 0, TileEdit.WIDTH, TileEdit.HEIGHT);
		notexture.graphics.moveTo(0, 0);
		notexture.graphics.lineTo(TileEdit.WIDTH, TileEdit.HEIGHT);
		notexture.graphics.moveTo(TileEdit.WIDTH, 0);
		notexture.graphics.lineTo(0, TileEdit.HEIGHT);
		notexture.buttonMode = true;
		notexture.addEventListener(MouseEvent.MOUSE_DOWN, (event:MouseEvent) -> {
			editor.mode = Tiles;
			selected = null;
			for (otherItem in items) {
				otherItem.selection.visible = false;
			}
		});
		notexture.y = height;
		addChild(notexture);
	}

	function select(event:MouseEvent) {
		editor.mode = Tiles;
		selected = cast(event.target, TextureItem).texture;

		for (otherItem in items) {
			otherItem.selection.visible = otherItem == event.target;
		}
	}
}

class MobsList extends Sprite {
	public var mob:String;

	public function new(editor:Main) {
		super();

		var types:Array<String> = ['player'];

		for (type in types) {
			var btn = new Button(type, () -> {
				trace('mob', type);
				editor.mode = Mobs;
			});

			btn.y = height;
			addChild(btn);
		}
	}
}

enum EditMode {
	Tiles;
	Mobs;
}

class Main extends Sprite {
	var filepath:String = null;

	public var assetsPath:String;

	public var textures:Array<String> = [];

	public var wallTexture:Textures;
	public var floorTexture:Textures;
	public var ceilTexture:Textures;

	public var bitmaps:Map<String, BitmapData> = [];

	public var field:Field;

	public var mode:EditMode = Tiles;

	public var mobs:MobsList;

	public function new() {
		super();

		assetsPath = Path.join([Sys.programPath(), '..', '..', '..', '..', '..', 'assets']).normalize();

		if (!assetsPath.exists())
			throw '$assetsPath - invalid path';

		for (file in Path.join([assetsPath, 'textures']).readDirectory()) {
			textures.push(file);
		}

		loadTextures();

		scaleX = scaleY = 2;

		var loadButton = new Button('load', () -> {
			var dialog = new FileDialog();
			dialog.onSelect.add((openPath:String) -> {
				load(openPath);
			});
			dialog.browse(OPEN, null, assetsPath);
		});

		var saveButton = new Button('save', () -> {
			if (filepath == null) {
				var saveDialog = new FileDialog();
				saveDialog.onSelect.add((newpath) -> {
					filepath = newpath;
					save();
				});
				saveDialog.browse(SAVE, null, assetsPath);
			} else {
				save();
			}
		});

		saveButton.x = loadButton.width + 10;

		addChild(loadButton);
		addChild(saveButton);

		field = new Field(this, 10, 10);
		field.y = loadButton.y + loadButton.height + 10;
		addChild(field);

		wallTexture = new Textures(this, 'Wall:', textures);
		wallTexture.x = field.x + field.width + 10;
		wallTexture.y = field.y;
		addChild(wallTexture);

		floorTexture = new Textures(this, 'Floor:', textures);
		floorTexture.x = wallTexture.x + wallTexture.width + 10;
		floorTexture.y = wallTexture.y;
		addChild(floorTexture);

		ceilTexture = new Textures(this, 'Ceiling:', textures);
		ceilTexture.x = floorTexture.x + floorTexture.width + 10;
		ceilTexture.y = floorTexture.y;
		addChild(ceilTexture);

		mobs = new MobsList(this);
		mobs.x = wallTexture.x;
		mobs.y = wallTexture.y + wallTexture.height + 10;
		addChild(mobs);
	}

	function loadTextures() {
		for (file in textures)
			bitmaps[file] = BitmapData.fromFile(Path.join([assetsPath, 'textures', file]));
	}

	function load(file:String) {
		trace('load', file);
		filepath = file;
		var map:MapDesc = cast Json.parse(File.getContent(file));

		textures = map.textures;

		loadTextures();

		var newField = new Field(this, map.mapWidth, map.mapHeight);
		newField.x = field.x;
		newField.y = field.y;
		newField.setTiles(map.tiles);

		removeChild(field);
		addChild(newField);

		field = newField;
	}

	function save() {
		trace('save', filepath);
		var map:MapDesc = {
			mapWidth: field.mapWidth,
			mapHeight: field.mapHeight,
			tiles: field.tileDescs,
			defaultCeilTextureID: null,
			defaultFloorTextureID: null,
			textures: textures,
			player: {
				pos: {
					x: 0,
					y: 0
				}
			},
			mobs: []
		};

		File.saveContent(filepath, Json.stringify(map));
	}
}
